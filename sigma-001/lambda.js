let AWS = require('aws-sdk');
const cognito_idp = new AWS.CognitoIdentityServiceProvider();

exports.handler = function (event, context, callback) {
    cognito_idp.listUsers({
        UserPoolId: "us-east-1_D10y3fy0o",
        AttributesToGet: ["sub", "given_name", "middle_name", "name", "family_name", "profile"],
        Filter: "Filter",
        Limit: 10
    }, function (error, data) {
        if (error) {
            callback(error);
        }
        callback(null, { "message": "Successfully executed!" });
    });
}